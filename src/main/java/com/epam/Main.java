/**
 * This is package com.epam.
 */
package com.epam;
/**
 * This is Main Class.
 */

import java.util.Scanner;

/**
 * Public Main Class.
 */
public class Main {
    /**
     * There is start point of program.
     *
     * @param args command line values.
     */
    public static void main(String[] args) {
        System.out.println("Enter the range of numbers: ");
        Scanner scan = new Scanner(System.in);
        OddEvenRange oddEvenRange = new OddEvenRange(scan.nextInt(), scan.nextInt());
        oddEvenRange.DisplayEvenOddNumbers();
        oddEvenRange.DisplaySumOfEvenOddNumbers();
        oddEvenRange.DisplayMaxEvenOddNumber();
        System.out.println("Now enter the size of set Fibonacci: ");
        oddEvenRange.FibonacciNumbers(scan.nextInt());
    }
}
