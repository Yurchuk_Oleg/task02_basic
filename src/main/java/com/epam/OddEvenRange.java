/**
 * This is package com.epam.
 */
package com.epam;

/***
 * @author Oleg
 * @version 1.1
 */
public class OddEvenRange {
    /**
     * This field realizes the beginning of the interval.
     */
    private int firstNumberOfRange;
    /**
     * This field realizes the end of the interval.
     */
    private int lastNumberOfRange;
    /**
     * This field realizes sum of odd numbers from interval.
     */
    private int sumOfOddNumbers = 0;
    /**
     * This field realizes sum of even numbers from interval.
     */
    private int sumOfEvenNumbers = 0;
    /**
     * This field realizes max number from all even numbers.
     */
    public int maxEvenNumber;
    /**
     * This field realizes max number from all odd numbers.
     */
    public int maxOddNumber;

    /**
     * This field will return the firstNumberOfRange value.
     *
     * @return firstNumberOfRange.
     */
    final public int getFirstNumberOfRange() {
        return firstNumberOfRange;
    }

    /**
     * This field will set firstNumberOfRange value.
     *
     * @param firstNumberOfRange value start of interval.
     */
    final public void setFirstNumberOfRange(int firstNumberOfRange) {
        this.firstNumberOfRange = firstNumberOfRange;
    }

    /**
     * This field will return the lastNumberOfRange value.
     *
     * @return lastNumberOfRange.
     */
    final public int getLastNumberOfRange() {
        return lastNumberOfRange;
    }

    /**
     * This field will set firstNumberOfRange value.
     *
     * @param lastNumberOfRange value end of interval.
     */
    final void setLastNumberOfRange(int lastNumberOfRange) {
        this.lastNumberOfRange = lastNumberOfRange;
    }

    /**
     * Another constructor for class OddEvenRange.
     *
     * @param firstNumberOfRange,lastNumberOfRange.
     */
    public OddEvenRange(int firstNumberOfRange, int lastNumberOfRange) {
        this.firstNumberOfRange = firstNumberOfRange;
        this.lastNumberOfRange = lastNumberOfRange;
    }

    /**
     * This is void method.
     * There are display even and odd numbers from invertal.
     */
    public void DisplayEvenOddNumbers() {
        int r = 0;
        System.out.println("Range:");
        System.out.println("From: " + firstNumberOfRange);
        System.out.println("To: " + lastNumberOfRange);
        System.out.print("\nEven numbers are: ");
        for (int i = firstNumberOfRange; i <= lastNumberOfRange; i++) {
            r = i % 2;
            if (r == 0) {
                sumOfEvenNumbers += i;
                System.out.print(i + " ");
                maxEvenNumber = i;
            }
        }
        System.out.print("\nOdd numbers are: ");
        for (int i = firstNumberOfRange; i <= lastNumberOfRange; i++) {
            r = i % 2;
            if (r == 1) {
                sumOfOddNumbers += i;
                System.out.print(i + " ");
                maxOddNumber = i;
            }
        }
    }

    /**
     * This is void method.
     * There are display sum of even and odd numbers.
     */
    final public void DisplaySumOfEvenOddNumbers() {
        System.out.println("\nThe sum of even numbers are: " + sumOfEvenNumbers);
        System.out.println("The sum of odd numbers are: " + sumOfOddNumbers);
    }

    /**
     * This is void method.
     * There are display max even and odd numbers.
     */
    final public void DisplayMaxEvenOddNumber() {
        System.out.println("Max even number: " + maxEvenNumber);
        System.out.println("Max odd number: " + maxOddNumber);
    }

    /**
     * This is void method.
     *
     * @param n - size of set Fibonacci which set user.*
     *          There are display first and second numbers of Fibonacci
     *          and all subsequent values of set.
     *          Display even and odd numbers and their count.
     *          Display percentage even and odd numbers of Fibonacci set.
     */
    final public void FibonacciNumbers(final int n) {
        final int ONE_HUNDRED = 100;
        final int sizeOfSet = n;
        final int f1Plusf2 = 3;
        int firstNumberOfFibonacci = maxOddNumber;
        int secondNumberOfFibonacci = maxEvenNumber;
        int nextNumberOfFibonacci = 0;
        int countEvenNumbersOfFib = 1;
        int countOddNumbersOfFib = 1;
        int r = 0;
        float perEvenFibo = 0;
        float perOddFibo = 0;
        System.out.println("First num of Fibonacci: " + firstNumberOfFibonacci);
        System.out.println("Second num of Fibonacci: " + secondNumberOfFibonacci);
        System.out.println("The size of set Fibonacci: " + sizeOfSet);
        System.out.println(firstNumberOfFibonacci);
        System.out.println(secondNumberOfFibonacci);
        for (int i = f1Plusf2; i <= sizeOfSet; i++) {
            nextNumberOfFibonacci = firstNumberOfFibonacci + secondNumberOfFibonacci;
            System.out.print(nextNumberOfFibonacci + "  ");
            firstNumberOfFibonacci = secondNumberOfFibonacci;
            secondNumberOfFibonacci = nextNumberOfFibonacci;
            r = nextNumberOfFibonacci % 2;
            if (r == 0) {
                countEvenNumbersOfFib++;
            } else {
                countOddNumbersOfFib++;
            }
        }
        System.out.println("\nEven: " + countEvenNumbersOfFib);
        System.out.println("Odd: " + countOddNumbersOfFib);
        perEvenFibo = (countEvenNumbersOfFib * ONE_HUNDRED) / sizeOfSet;
        perOddFibo = (countOddNumbersOfFib * ONE_HUNDRED) / sizeOfSet;
        System.out.println("even nums: " + perEvenFibo + "%");
        System.out.println("odd nums: " + perOddFibo + "%");
    }
}
